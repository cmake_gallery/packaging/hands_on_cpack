#include "Projection/stereographic_projection.hpp"

#include <iostream>
#include <vector>
#include <cstddef>
#include <stdexcept>
// Enable asserts even in release mode
#if defined(NDEBUG)
#undef NDEBUG
#endif
#include <cassert>

int main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) {
  std::vector<std::vector<double>> point_cloud {{4., 3., 2., 1.},
                                                {5., 6., 2., 1.}};

  auto proj = stereographic_projection(point_cloud);

  assert(point_cloud.size() == proj.size());
  for (std::size_t i = 0; i < proj.size(); ++i) {
    assert(point_cloud[i].size() == proj[i].size() + 1);
    for (std::size_t j = 0; j < proj[i].size(); ++j) {
      std::clog << proj[i][j] << " ,";
        assert(proj[i][j] == point_cloud[i][j]);
    }
    std::clog << "\n";
  }
    
  std::vector<std::vector<double>> point_cloud_in_error {{4., 3., 2., 1.},
                                                         {5., 6., 2., 1.},
                                                         {7., 8., 2., 0.}, // shall throw an exception
                                                         {9., 1., 2., 1.}};

  try {
    auto proj_in_error = stereographic_projection(point_cloud_in_error);
    // Should not go further
    assert(false);
  } catch (const std::invalid_argument& e) {
    std::clog << "Exception: " << e.what() << std::endl;
  }
  
  return EXIT_SUCCESS;
}
