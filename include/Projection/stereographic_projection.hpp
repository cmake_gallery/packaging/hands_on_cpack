#include <stdexcept>
#include <algorithm>  // for std::transform
#include <vector>
#include <limits>     // for std::numeric_limits
#include <cmath>      // for std::fabs

std::vector<std::vector<double>> stereographic_projection(const std::vector<std::vector<double>> &point_cloud) {
  std::vector<std::vector<double>> proj;
  for (const auto &point : point_cloud) {
    double last_dim_value = point.back();
    if (std::fabs(last_dim_value) <= std::numeric_limits<double>::epsilon())
      throw std::invalid_argument("Division by zero");
    std::vector<double> proj_point;
    std::transform(point.begin(), point.end() - 1, std::back_inserter(proj_point), [&last_dim_value](double val) { return val / last_dim_value; });
    proj.push_back(proj_point);
  }
  return proj;
} 