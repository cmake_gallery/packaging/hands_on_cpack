# hands_on_cpack

## Acknowledgement

This tutorial has been inspired by [CMake line by line - creating a header-only library](https://dominikberner.ch/cmake-interface-lib/).
Please refer to it and to [SI - Type safety for physical units -](https://github.com/bernedom/SI/) if you need a more complete example.

## C++ standard

As the code is using [std::transform](https://en.cppreference.com/w/cpp/algorithm/transform), the required C++ standard is C++17.

## Header only library

A project that is only composed of C++ header files (these files may be organized through directories) is called a header only library.
This is the case of this project which is composed of a single header file that contains a single function `stereographic_projection`.

## CMake

First cmake needs to be launched to initialize the project:

```bash
mkdir build
cd build
cmake ..
```
(We will stay in this build directory)

You can see that there is nothing to build:
```bash
make
```

## Unitary tests

Even if the library is header only, the project contains unitary tests that are testing the function.
These tests will not be packaged with the library, they are just here for the developers to test their modifications.
The end-users will only need the header files to be copied.

To build the unitary tests, we need to set an `option`:
```bash
cmake -DTESTS=ON ..
make
# [ 50%] Building CXX object test/CMakeFiles/test_stereographic_projection.dir/test_stereographic_projection.cpp.o
# [100%] Linking CXX executable test_stereographic_projection
# [100%] Built target test_stereographic_projection
```

We can also verify that all the tests are passed:
```bash
ctest
# Test project /home/gailuron/workspace/formations/hands_on_cpack/build
#     Start 1: test_stereographic_projection
# 1/1 Test #1: test_stereographic_projection ....   Passed    0.00 sec
# 
# 100% tests passed, 0 tests failed out of 1
# 
# Total Test time (real) =   0.00 sec
```


## CPack

[CPack](https://cmake.org/cmake/help/latest/module/CPack.html) is CMake util (just like ctest) that allows to configure
directly in your `CMakeLists.txt` the way you want to pack your library.

You can pack header only libraries (our case) that will copy your header files in the correct system path.
But you could also copy (with or without the header files) dynamic and/or static libraries, and/or executables.

CPack has several possible generators that you can configure directly with the command line:
```bash
cpack -G TGZ
# CPack: Create package using TGZ
# CPack: Install projects
# CPack: - Run preinstall target for: Projection
# CPack: - Install project: Projection []
# CPack: Create package
# CPack: - package: ./Projection-0.0.1-Linux.tar.gz generated.
```

The package `./Projection-0.0.1-Linux.tar.gz` contains:
```
/Projection-0.0.1-Linux/include/Projection/stereographic_projection.hpp
```

**Note:** It would contain a `bin` folder if there were some executables, and a `lib` folder for possible dynamic
and/or static libraries.

These are the files that have to be copied in the system installation folder, typically `/usr/local` on a linux/unix
system.

## Installation

We have seen how to make a package thanks to CPack, but that's all the things we enabled with the only few lines of
CMake that was written.

The library is now installable with the command:
```bash
make install
# [100%] Built target test_stereographic_projection
# Install the project...
# -- Install configuration: "Release"
# -- Installing: /usr/local/include/Projection
# CMake Error at cmake_install.cmake:51 (file):
#   file INSTALL cannot make directory "/usr/local/include/Projection":
#   Permission denied.
# 
# 
# make: *** [Makefile:130: install] Error 1
```

Here it fails because default `CMAKE_INSTALL_PREFIX` is `/usr/local` and requires super-user privilege to install here,
but `sudo make install` would do the job.